package components.characters

import components.Component
import java.awt.Image

trait Character extends Component {
  def counter: Int

  def counter_=(counter: Int): Unit

  def isAlive: Boolean

  def isAlive_=(alive: Boolean): Unit

  def isMoving: Boolean

  def isMoving_=(moving: Boolean): Unit

  def isToRight: Boolean

  def isToRight_=(toRight: Boolean): Unit

  def walk(name: String, frequency: Int): Image

  def isNearby(component: Component): Boolean
}

object Character{
  def apply(characterX: Int, characterY: Int, characterWidth: Int, characterHeight: Int):Component = new BasicCharacter(characterX, characterY, characterWidth, characterHeight)
}

import components.GameComponent
import utils.Res
import utils.Utils

object BasicCharacter {
  private val PROXIMITY_MARGIN = 10
  private val HIT_MARGIN = 5
}
import BasicCharacter._
class BasicCharacter( characterX: Int,  characterY: Int, characterWidth: Int, characterHeight: Int) extends GameComponent(characterX, characterY, characterWidth, characterHeight) with Character {

  override var counter: Int = 0
  override var isAlive: Boolean = true
  override var isMoving: Boolean = false
  override var isToRight: Boolean = true

  override def walk(name: String, frequency: Int) = {
    val stringWalk = Res.IMG_BASE + name + (if (!this.isMoving || {
        this.counter += 1
        this.counter
      } % frequency == 0) Res.IMGP_STATUS_ACTIVE
      else Res.IMGP_STATUS_NORMAL) + (if (this.isToRight) Res.IMGP_DIRECTION_DX
      else Res.IMGP_DIRECTION_SX) + Res.IMG_EXT
      Utils getImage stringWalk
  }

  override def isNearby(component: Component) = (this.x > component.x - PROXIMITY_MARGIN && this.x < component.x + component.width + PROXIMITY_MARGIN) || (this.x + this.width > component.x - PROXIMITY_MARGIN && this.x + this.width < component.x + component.width + PROXIMITY_MARGIN)

  protected def hitAhead(component: Component): Boolean = component match {
    case component: Character if(!this.isToRight) => false
    case _ => {
      !(this.x + this.width < component.x ||
        this.x + this.width > component.x + HIT_MARGIN ||
        this.y + this.height <= component.y || this.y >= component.y + component.height)
    }
  }

  protected def hitBack(component: Component): Boolean = !(this.x > component.x + component.width || this.x + this.width < component.x + component.width - HIT_MARGIN || this.y + this.height <= component.y || this.y >= component.y + component.height)

  import components.objects.GameObject

  protected def hitBelow(component: Component): Boolean = {
    def hitBelowControl(component: Component, margin: Int): Boolean = !(this.x + this.width < component.x + margin || this.x > component.x + component.width - margin || this.y + this.height < component.y || this.y + this.height > component.y + margin)

    component match {
      case component: GameObject => hitBelowControl(component, HIT_MARGIN)
      case _ => hitBelowControl(component, 0)
    }
  }
  protected def hitAbove(component: Component): Boolean = !(this.x + this.width < component.x + HIT_MARGIN || this.x > component.x + component.width - HIT_MARGIN || this.y < component.y + component.height || this.y > component.y + component.height + HIT_MARGIN)
}
