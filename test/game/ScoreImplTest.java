package game;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Giulia on 16/03/2017.
 */
public class ScoreImplTest {

    private Score score;
    private ScoreStrategy strategy;

    @Before
    public void init(){
        this.score = new ScoreImpl();
    }

    @Test
    public void isScoreStartedToZero() throws Exception {
        assertTrue(this.score.getScore() == 0);
    }

    @Test
    public void isScoreIncrementedWithMoney() throws Exception {
        this.score.setScoreStrategy(new MoneyScoreStrategy());
        this.score.increment();
        this.score.increment();
        assertTrue(this.score.getScore() == 2);
    }

    @Test
    public void isScoreIncrementedWithEnemy() throws Exception {
        this.score.setScoreStrategy(new EnemyScoreStrategy());
        this.score.increment();
        this.score.increment();
        assertTrue(this.score.getScore() == 20);
    }

    @Test
    public void isScoreResetted() throws Exception {
        this.score.setScoreStrategy(new EnemyScoreStrategy());
        this.score.increment();
        this.score.increment();
        this.score.reset();
        assertTrue(this.score.getScore() == 0);
    }

    @Test
    public void incrementResetCombination() throws Exception {
        this.score.setScoreStrategy(new EnemyScoreStrategy());
        for(int i=0; i<5; i++) {
            this.score.increment();
            this.score.reset();
        }
        assertTrue(this.score.getScore() == 0);
    }


}