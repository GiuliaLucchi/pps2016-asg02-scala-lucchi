package components.characters

import java.awt.Image

import components.Component
import components.objects.{Object, RedMushroom}

/**
  * Created by Giulia on 09/04/2017.
  */

trait Mario extends Character{

  def lives: Int

  def lives_=(lives: Int): Unit

  def isJumping: Boolean

  def isJumping_=(jumping: Boolean): Unit

  def doJump(): Image

  def checkContact(`object`: Object): Boolean

  def contactWithObject(`object`: Component): Unit

  def contactWithCharacter(character: Component): Unit
}

object Mario{
  def apply(x: Int, y: Int): Mario = new MarioImpl(x, y)
}

object MarioImpl {
  private val MARIO_OFFSET_Y_INITIAL = 243
  private val MARIO_OFFSET_Y_MINI = 258
  private val FLOOR_OFFSET_Y_INITIAL = 293
  private val WIDTH = 28
  private val HEIGHT = 50
  private val HEIGHT_MINI = 35
  private val JUMPING_LIMIT = 42
  private val MARIO_LIVES = 2
}

import game.Main
import utils.Res
import utils.Utils
import MarioImpl._

class MarioImpl(marioX: Int, marioY: Int) extends BasicCharacter(marioX, marioY, WIDTH, HEIGHT) with Mario{

  private var imageMario = Utils getImage Res.IMG_MARIO_DEFAULT
  private var jumpingExtent = 0
  private var isImmortal = true

  override var lives = MARIO_LIVES

  override var isJumping = false

  override def doJump() = {

    def defineStringJump(marioLives: Int): String = marioLives match {
        case 2 => {
          var stringJump = if (this.isToRight) Res.IMG_MARIO_SUPER_DX
          else Res.IMG_MARIO_SUPER_SX
          stringJump
        }
        case 1 => {
          var stringJump = if (this.isToRight) Res.IMG_MARIO_SUPER_DX_MINI
          else Res.IMG_MARIO_SUPER_SX_MINI
          stringJump
        }
        case _ => ""
    }

    var stringJump: String = null
    this.jumpingExtent += 1
    if (this.jumpingExtent < JUMPING_LIMIT) {
      if (this.y > Main.scene.heightLimit) this.y = this.y - 4
      else this.jumpingExtent = JUMPING_LIMIT
      stringJump = defineStringJump(this.lives)
    }
    else if (this.y + this.height < Main.scene.floorOffsetY) {
      this.y = this.y + 1
      stringJump = defineStringJump(this.lives)
    }
    else {
      stringJump = defineStringJump(this.lives)
      this.isJumping = false
      this.jumpingExtent = 0
    }
    Utils getImage stringJump
  }

  override def checkContact(`object`: Object) = this.hitBack(`object`) || this.hitAbove(`object`) || this.hitAhead(`object`) || this.hitBelow(`object`)

  override def contactWithObject(obj: Component) = obj match {
    case _: RedMushroom => {
      if (this.lives == 1) {
        this.lives = this.lives + 1
        this.imageMario = Utils getImage Res.IMG_MARIO_DEFAULT
        this.y = MARIO_OFFSET_Y_INITIAL
        this.height = HEIGHT
      }
    }
    case _ => {
      if (this.hitAhead(obj) && this.isToRight || this.hitBack(obj) && !this.isToRight) {
        Main.scene.movement = 0
        this.isMoving = false
      }
      if (this.hitBelow(obj) && this.isJumping) Main.scene.floorOffsetY = obj.y
      else if (!this.hitBelow(obj)) {
        Main.scene.floorOffsetY = FLOOR_OFFSET_Y_INITIAL
        if (!this.isJumping) {
          if (this.lives == 2) this.y = MARIO_OFFSET_Y_INITIAL
          else if (this.lives == 1) this.y = MARIO_OFFSET_Y_MINI
        }
        if (hitAbove(obj)) Main.scene.heightLimit = obj.y + obj.height // the new sky goes below the object
        else if (!this.hitAbove(obj) && !this.isJumping) Main.scene.heightLimit = 0 // initial sky
      }
    }
  }

  import game.EnemyScoreStrategy

  override def contactWithCharacter(character: Component) = {

    def updateMarioStatus(marioLives: Int) = marioLives match {
      case 2 => {
        this.lives = this.lives - 1
        this.imageMario = Utils getImage  Res.IMG_MARIO_DEFAULT_MINI
        this.y = MARIO_OFFSET_Y_MINI
        this.height = HEIGHT_MINI
        new Thread(() => {
          this.isImmortal = true
          Thread.sleep(1000)
          this.isImmortal = false
        }).start()
      }
      case 1 if !this.isImmortal => {
        this.isMoving = false
        this.isAlive = false
      }
      case _ =>
    }

    if (this.hitAhead(character) || this.hitBack(character))
      if (character.asInstanceOf[Character].isAlive) updateMarioStatus(this.lives)

    else this.isAlive = true
    else if (this.hitBelow(character)) {
      character.asInstanceOf[Character].isMoving = false
      character.asInstanceOf[Character].isAlive = false
      Main.scene.updateScore(new EnemyScoreStrategy)
    }
  }
}