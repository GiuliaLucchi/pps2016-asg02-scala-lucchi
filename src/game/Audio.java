package game;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public class Audio {
    private Clip clip;

    public Audio(String song) {

        try {
            AudioInputStream audio = AudioSystem.getAudioInputStream(getClass().getResource(song));
            clip = AudioSystem.getClip();
            clip.open(audio);
        } catch (Exception e) {
            // TODO: log error
        }
    }

    public Clip getClip() {
        return clip;
    }

    public void play() {
        clip.start();
    }

    public void stop() {
        clip.stop();
    }

    public static void playSound(String song) {
        Audio audio = new Audio(song);
        audio.play();
    }

}
