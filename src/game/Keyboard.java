package game;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Keyboard implements KeyListener {

    @Override
    public void keyPressed(KeyEvent e) {

        if (Main.scene.mario().isAlive()) {
            if (e.getKeyCode() == KeyEvent.VK_RIGHT) {

                // per non fare muovere il castello e start
                if (Main.scene.xPosition() == -1) {
                    Main.scene.xPosition_$eq(0);
                    Main.scene.background1XPosition_$eq(-50);
                    Main.scene.background2XPosition_$eq(750);
                }
                Main.scene.mario().isMoving_$eq(true);
                Main.scene.mario().isToRight_$eq(true);
                Main.scene.movement_$eq(1); // si muove verso sinistra
            } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {

                if (Main.scene.xPosition() == 4601) {
                    Main.scene.xPosition_$eq(4600);
                    Main.scene.background1XPosition_$eq(-50);
                    Main.scene.background2XPosition_$eq(750);
                }

                Main.scene.mario().isMoving_$eq(true);
                Main.scene.mario().isToRight_$eq(false);
                Main.scene.movement_$eq(-1); // si muove verso destra
            }
            // salto
            if (e.getKeyCode() == KeyEvent.VK_UP) {
                Main.scene.mario().isJumping_$eq(true);
                Audio.playSound("/resources/audio/jump.wav");
            }

        }else{
            if (e.getKeyCode() == KeyEvent.VK_Z) {
                Main.window.setVisible(false);
                String[] args = {};
                Main.main(args);
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        Main.scene.mario().isMoving_$eq(false);
        Main.scene.movement_$eq(0);
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

}
