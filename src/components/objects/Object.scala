package components.objects

import components.Component
import components.GameComponent
import java.awt.Image
import utils.Res

/**
  * Created by Giulia on 12/03/2017.
  */
trait Object extends Component {
  def imageObject: Image
}

object Object {
  def apply(objectName: String, x: Int, y: Int):Object = objectName match {
    case Res.OBJECT_BLOCK => new Block(x, y)
    case Res.OBJECT_TUNNEL => new Tunnel(x, y)
    case Res.OBJECT_MONEY => new Money(x, y)
    case Res.OBJECT_RED_MUSHROOM => new RedMushroom(x, y)
  }
}

class GameObject(x: Int, y: Int, width: Int, height: Int, override val imageObject: Image) extends GameComponent(x, y, width, height) with Object {}

import utils.Utils

object Block {
  private val WIDTH = 30
  private val HEIGHT = 30
  private val IMAGE = Utils getImage Res.IMG_BLOCK
}

import Block._
class Block(x: Int, y: Int) extends GameObject(x, y, WIDTH, HEIGHT, IMAGE) {}

object Tunnel {
  private val WIDTH = 43
  private val HEIGHT = 65
  private val IMAGE = Utils getImage Res.IMG_TUNNEL
}

import Tunnel._
class Tunnel(x: Int, y: Int) extends GameObject(x, y, WIDTH, HEIGHT, IMAGE) {}

object Money {
  private val WIDTH = 30
  private val HEIGHT = 30
  private val PAUSE = 10
  private val FLIP_FREQUENCY = 100
  private val IMAGE = Utils getImage Res.IMG_PIECE1
}

import Money._
class Money(x: Int, y: Int) extends GameObject(x, y, WIDTH, HEIGHT, IMAGE) with Runnable {

  private var counter = 0

  def imageOnMovement(): Image = Utils.getImage(
    if ( { this.counter += 1; this.counter  } % Money.FLIP_FREQUENCY == 0) Res.IMG_PIECE1 else Res.IMG_PIECE2)

  override def run() {
    while (true) {
      imageOnMovement()
      try Thread sleep Money.PAUSE
      catch {
        case e: InterruptedException => println(e)
      }
    }
  }
}

object RedMushroom {
  private val WIDTH = 25
  private val HEIGHT = 25
  private val IMAGE = Utils getImage Res.IMG_RED_MUSHROOM
}

import RedMushroom._
class RedMushroom(x: Int, y: Int) extends GameObject(x, y, WIDTH, HEIGHT, IMAGE) {}

