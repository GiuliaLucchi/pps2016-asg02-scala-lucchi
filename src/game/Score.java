package game;

/**
 * Created by Giulia on 15/03/2017.
 */
public interface Score {

    int getScore();

    void reset();

    void increment();

    void setScoreStrategy(ScoreStrategy strategy);

}
