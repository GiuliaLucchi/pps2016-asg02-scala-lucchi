package game;

import javax.swing.JFrame;

public class Main {

    private static final int WINDOW_WIDTH = 700;
    private static final int WINDOW_HEIGHT = 360;
    private static final String WINDOW_TITLE = "Super Mario";
    public static PlatformImpl scene;
    public static Refresh refresh;
    public static JFrame window;

    public static void main(String[] args) {
        window = new JFrame(WINDOW_TITLE);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        window.setLocationRelativeTo(null);
        window.setResizable(true);
        window.setAlwaysOnTop(true);

        scene = new PlatformImpl();
        window.setContentPane(scene);
        window.setVisible(true);

        refresh = new Refresh();
        Thread timer = new Thread(refresh);
        timer.start();
    }

}
