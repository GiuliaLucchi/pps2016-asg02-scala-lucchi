package components.characters;

import components.objects.Object;
import components.objects.Tunnel;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Giulia on 16/03/2017.
 */
public class AbstractEnemyTest {

    private final static int MUSHROOM_X_POSITION = 0;
    private final static int MUSHROOM_Y_POSITION = 0;
    private final static int TURTLE_X_POSITION = 50;
    private final static int TURTLE_Y_POSITION = 0;
    private final static int TUNNEL_X_POSITION = 40;
    private final static int TUNNEL_Y_POSITION = 0;

    private Enemy mushroom;
    private Enemy turtle;
    private Object tunnel;

    @Before
    public void init(){
        this.mushroom = new Mushroom(MUSHROOM_X_POSITION, MUSHROOM_Y_POSITION);
        this.turtle = new Turtle(TURTLE_X_POSITION, TURTLE_Y_POSITION);
        this.tunnel = new Tunnel(TUNNEL_X_POSITION, TUNNEL_Y_POSITION);
    }

    @Test
    public void isEnemyMovedToRight() throws Exception {
        this.mushroom.isToRight_$eq(true);
        this.mushroom.moveEnemy();
        assertTrue(this.mushroom.x() > MUSHROOM_X_POSITION);
    }

    @Test
    public void isEnemyMovedToLeft() throws Exception {
        this.mushroom.isToRight_$eq(true);
        this.mushroom.moveEnemy();
        int x1 = this.mushroom.x();
        this.mushroom.isToRight_$eq(false);
        this.mushroom.moveEnemy();
        int x2 = this.mushroom.x();
        assertTrue(x2 < x1);
    }

    @Test
    public void isChangedDirectionAfterContactWithAnotherEnemy() throws Exception {
       this.mushroom.isToRight_$eq(true);
       this.turtle.isToRight_$eq(false);
       for(int i=0; i<15; i++){
           this.mushroom.moveEnemy();
           this.turtle.moveEnemy();
           this.mushroom.contact(this.turtle);
           this.turtle.contact(this.mushroom);
        }
        assertFalse(this.mushroom.isToRight());
        assertTrue(this.turtle.isToRight());
    }

    @Test
    public void isChangedDirectionAfterContactWithAnObject() throws Exception {
        this.mushroom.isToRight_$eq(true);
        for(int i=0; i<15; i++){
            this.mushroom.moveEnemy();
            this.mushroom.contact(this.tunnel);
        }
        assertFalse(this.mushroom.isToRight());
    }

}