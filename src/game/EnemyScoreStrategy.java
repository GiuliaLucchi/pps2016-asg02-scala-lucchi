package game;

/**
 * Created by Giulia on 16/03/2017.
 */
public class EnemyScoreStrategy implements ScoreStrategy {

    @Override
    public int doIncrement(int score) {
        return score + 10;
    }
}
