package game;

/**
 * Created by Giulia on 16/03/2017.
 */
public class ScoreImpl implements Score {

    private int score;
    private ScoreStrategy strategy;

    public ScoreImpl(){
        this.score = 0;
    }

    @Override
    public int getScore() {
        return this.score;
    }

    @Override
    public void increment() {
        this.score = this.strategy.doIncrement(score);
    }

    @Override
    public void reset() {
        this.score = 0;
    }

    public void setScoreStrategy(ScoreStrategy strategy){
        this.strategy = strategy;
    }
}
