package components.characters

import java.awt.Image

import components.Component
import utils.Res

/**
  * Created by Giulia on 13/03/2017.
  */
trait Enemy extends Character {
  def image: Image

  def moveEnemy(): Unit

  def contact(component: Component): Unit

  def deadImage: Image
}

object Enemy{
  def apply(enemy: String, x: Int, y: Int):Enemy = enemy match {
    case Res.CHARACTER_MUSHROOM => new Mushroom(x, y)
    case Res.CHARACTER_TURTLE => new Turtle(x, y)
  }
}

object AbstractEnemy{
  private val PAUSE = 15
}

abstract class AbstractEnemy( enemyX: Int, enemyY: Int, enemyWidth: Int, enemyHeight: Int, override val image: Image) extends BasicCharacter(enemyX, enemyY, enemyWidth, enemyHeight) with Runnable with Enemy {

  override val deadImage = this.getDeadImage

  private var offsetX = 1

  this.isToRight = true
  this.isMoving = true

  private val chronoEnemy = new Thread(this)
  chronoEnemy.start()

  override def run() = {
    while ( this.isAlive ) {
      this.moveEnemy()
      try Thread sleep AbstractEnemy.PAUSE
      catch {
        case e: InterruptedException => println(e)
      }
    }
  }

  override def moveEnemy() = {
    this.offsetX = if (this.isToRight) 1 else -1
    this.x = this.x + this.offsetX
  }

  override def contact(component: Component) = {
    if (this.hitAhead(component) && this.isToRight) {
      this.isToRight = false
      this.offsetX = -1
    }
    else if (this.hitBack(component) && !this.isToRight) {
      this.isToRight = true
      this.offsetX = 1
    }
  }

  protected def getDeadImage: Image
}

import utils.Res
import utils.Utils

object Mushroom {
  private val WIDTH = 27
  private val HEIGHT = 30
}

class Mushroom(mushroomX: Int, mushroomY: Int) extends AbstractEnemy(mushroomX, mushroomY, Mushroom.WIDTH, Mushroom.HEIGHT, Utils.getImage(Res.IMG_MUSHROOM_DEFAULT)) {

  override protected def getDeadImage = Utils getImage (if (this.isToRight) Res.IMG_MUSHROOM_DEAD_DX  else Res.IMG_MUSHROOM_DEAD_SX)
}

object Turtle {
  private val WIDTH = 43
  private val HEIGHT = 50
}

class Turtle( turtleX: Int, turtleY: Int) extends AbstractEnemy(turtleX, turtleY, Turtle.WIDTH, Turtle.HEIGHT, Utils.getImage(Res.IMG_TURTLE_IDLE)) {

  override protected def getDeadImage = Utils getImage Res.IMG_TURTLE_DEAD
}