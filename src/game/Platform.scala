package game


import components.characters._


trait Platform{
  def mario: Mario

  def mario_=(mario: Mario): Unit

  def floorOffsetY: Int

  def floorOffsetY_=(floorOffsetY: Int): Unit

  def heightLimit: Int

  def heightLimit_=(heightLimit: Int): Unit

  def movement: Int

  def movement_=(movement: Int): Unit

  def xPosition: Int

  def xPosition_=(xPosition: Int): Unit

  def background1XPosition: Int

  def background1XPosition_=(background1XPosition: Int): Unit

  def background2XPosition: Int

  def background2XPosition_=(background2XPosition: Int): Unit

  def updateBackgroundOnMovement(): Unit

  def updateScore(strategy: ScoreStrategy): Unit
}

object Platform{
  def apply():Platform = new PlatformImpl()
}

object PlatformImpl {
  private val MARIO_FREQUENCY = 25
  private val MUSHROOM_FREQUENCY = 45
  private val TURTLE_FREQUENCY = 45
  private val MUSHROOM_DEAD_OFFSET_Y = 20
  private val TURTLE_DEAD_OFFSET_Y = 30
  private val FLAG_X_POS = 4650
  private val CASTLE_X_POS = 4850
  private val FLAG_Y_POS = 115
  private val CASTLE_Y_POS = 145
  private val GAME_OVER_X_POSITION = 310
  private val GAME_OVER_Y_POSITION = -40
  private val TRY_AGAIN_X_POSITION = 55
  private val TRY_AGAIN_Y_POSITION = 240
  private val SCORE_X_POSITION = 600
  private val SCORE_Y_POSITION = 30
}

import components.objects._
import javax.swing.JPanel
import utils.Res
import utils.Utils
import PlatformImpl._

class PlatformImpl() extends JPanel with Platform {

  private var imageBackground1 = Utils getImage Res.IMG_BACKGROUND
  private var imageBackground2 = Utils getImage Res.IMG_BACKGROUND
  private var imageGameOver = Utils getImage Res.IMG_GAME_OVER
  private var imageTryAgain = Utils getImage Res.IMG_TRY_AGAIN
  private var castle = Utils getImage Res.IMG_CASTLE
  private var start = Utils getImage Res.START_ICON
  private var imageFlag = Utils getImage Res.IMG_FLAG
  private var imageCastle = Utils getImage Res.IMG_CASTLE_FINAL

  private var score = new ScoreImpl

  private var objects: Seq[Object] = Seq[Object]()
  private var money: Seq[Money] = Seq[Money]()
  private var enemies: Seq[Enemy] = Seq[Enemy]()

  override var mario: Mario = _

  override var floorOffsetY = 293

  override var heightLimit = 0

  override var movement = 0

  override var xPosition = -1

  override var background1XPosition = -50

  override var background2XPosition = 750

  this.setFocusable(true)
  this.requestFocusInWindow
  this.addKeyListener(new Keyboard)

  this.createComponent()

  private def createComponent() = {
    this.createMario()
    this.createEnemies()
    this.createObjects()
  }

  private def createMario() = this.mario = Mario(300, 245)

  private def createEnemies() = {
    this.createMushrooms()
    this.createTurtles()
  }

  private def createMushrooms() = {
    this.enemies = this.enemies :+ Enemy(Res.CHARACTER_MUSHROOM, 800, 263)
    this.enemies = this.enemies :+ Enemy(Res.CHARACTER_MUSHROOM, 1200, 263)
    this.enemies = this.enemies :+ Enemy(Res.CHARACTER_MUSHROOM, 2600, 263)
  }

  private def createTurtles() = {
    this.enemies = this.enemies :+ Enemy(Res.CHARACTER_TURTLE, 950, 243)
    this.enemies = this.enemies :+ Enemy(Res.CHARACTER_TURTLE, 1600, 243)
    this.enemies = this.enemies :+ Enemy(Res.CHARACTER_TURTLE, 3900, 243)
  }

  private def createObjects() = {
    this.createTunnels()
    this.createBlocks()
    this.createMoney()
    this.createRedMushroom()
  }

  private def createTunnels() = {
    this.objects = this.objects :+ Object(Res.OBJECT_TUNNEL, 600, 230)
    this.objects = this.objects :+ Object(Res.OBJECT_TUNNEL, 1000, 230)
    this.objects = this.objects :+ Object(Res.OBJECT_TUNNEL, 1900, 230)
    this.objects = this.objects :+ Object(Res.OBJECT_TUNNEL, 2500, 230)
    this.objects = this.objects :+ Object(Res.OBJECT_TUNNEL, 3000, 230)
    this.objects = this.objects :+ Object(Res.OBJECT_TUNNEL, 3800, 230)
    this.objects = this.objects :+ Object(Res.OBJECT_TUNNEL, 4500, 230)
  }

  private def createBlocks() = {
    this.objects = this.objects :+ Object(Res.OBJECT_BLOCK, 400, 180)
    this.objects = this.objects :+ Object(Res.OBJECT_BLOCK, 1200, 180)
    this.objects = this.objects :+ Object(Res.OBJECT_BLOCK, 1270, 170)
    this.objects = this.objects :+ Object(Res.OBJECT_BLOCK, 1340, 160)
    this.objects = this.objects :+ Object(Res.OBJECT_BLOCK, 2000, 180)
    this.objects = this.objects :+ Object(Res.OBJECT_BLOCK, 2600, 160)
    this.objects = this.objects :+ Object(Res.OBJECT_BLOCK, 2650, 180)
    this.objects = this.objects :+ Object(Res.OBJECT_BLOCK, 3500, 160)
    this.objects = this.objects :+ Object(Res.OBJECT_BLOCK, 3550, 140)
    this.objects = this.objects :+ Object(Res.OBJECT_BLOCK, 4000, 170)
    this.objects = this.objects :+ Object(Res.OBJECT_BLOCK, 4200, 200)
    this.objects = this.objects :+ Object(Res.OBJECT_BLOCK, 4300, 210)
  }

  private def createMoney() = {
    this.money = this.money :+ Object(Res.OBJECT_MONEY, 402, 145).asInstanceOf[Money]
    this.money = this.money :+ Object(Res.OBJECT_MONEY, 1202, 140).asInstanceOf[Money]
    this.money = this.money :+ Object(Res.OBJECT_MONEY, 1272, 95).asInstanceOf[Money]
    this.money = this.money :+ Object(Res.OBJECT_MONEY, 1342, 40).asInstanceOf[Money]
    this.money = this.money :+ Object(Res.OBJECT_MONEY, 1650, 145).asInstanceOf[Money]
    this.money = this.money :+ Object(Res.OBJECT_MONEY, 2650, 145).asInstanceOf[Money]
    this.money = this.money :+ Object(Res.OBJECT_MONEY, 3000, 135).asInstanceOf[Money]
    this.money = this.money :+ Object(Res.OBJECT_MONEY, 3400, 125).asInstanceOf[Money]
    this.money = this.money :+ Object(Res.OBJECT_MONEY, 4200, 145).asInstanceOf[Money]
    this.money = this.money :+ Object(Res.OBJECT_MONEY, 4600, 40).asInstanceOf[Money]
  }

  private def createRedMushroom() = {
    this.objects = this.objects :+ Object(Res.OBJECT_RED_MUSHROOM,2200, 150)
  }

  override def updateBackgroundOnMovement() = {
    if (this.xPosition >= 0 && this.xPosition <= 4600) {
      this.xPosition = this.xPosition + this.movement
      // Moving the screen to give the impression that Mario is walking
      this.background1XPosition = this.background1XPosition - this.movement
      this.background2XPosition = this.background2XPosition - this.movement
    }
    this.flipBackground()
  }

  private def flipBackground() = { // Flipping between background1 and background2
    if (this.background1XPosition == -800) this.background1XPosition = 800
    else if (this.background2XPosition == -800) this.background2XPosition = 800
    else if (this.background1XPosition == 800) this.background1XPosition = -800
    else if (this.background2XPosition == 800) this.background2XPosition = -800
  }

  import java.awt.Graphics
  import java.awt.Graphics2D

  override def paintComponent(graphic: Graphics) = {
    super.paintComponent(graphic)

    val graphic2D: Graphics = graphic.asInstanceOf[Graphics2D]

    this.contactWithObject()
    this.checkContactWithMoney()
    this.contactWithCharacter()
    this.moveFixedComponents()
    this.drawBackgrounds(graphic2D)
    this.drawCastle(graphic2D)
    this.drawStart(graphic2D)
    this.drawObjects(graphic2D)
    this.drawMoney(graphic2D)
    this.drawImageFlag(graphic2D)
    this.drawImageCastle(graphic2D)
    this.drawScore(graphic2D)
    this.drawMario(graphic2D)
    this.drawEnemies(graphic2D)
    this.checkMarioDeath(graphic2D)
  }

  private def contactWithObject() = {
    this.objects foreach (obj => { obj match {
        case obj: RedMushroom => {
          if( this.mario checkContact obj) {
            this.mario contactWithObject obj
            this.objects = this.objects filter (_ != obj)
          }
        }
        case _ => {
          if (this.mario isNearby obj)
            this.mario contactWithObject obj
        }
      }

      this.enemies foreach (enemy => {
      if (enemy.isAlive & (enemy isNearby obj)) enemy contact obj
      })
    })
  }

  private def checkContactWithMoney() = {
    this.money foreach (m => {
      if (this.mario checkContact  m) {
        Audio playSound Res.AUDIO_MONEY
        this.money = this.money filter (_!=m)
        this.updateScore(new MoneyScoreStrategy)
      }
    })
  }

  private def contactWithCharacter() = {
    this.enemies foreach (enemy1 => {
      if (enemy1.isAlive) {
        this.enemies foreach (enemy2 => {
          if ((enemy1 != enemy2) && (enemy1 isNearby enemy2)) enemy1 contact enemy2
        })
        if (this.mario isNearby enemy1) this.mario contactWithCharacter enemy1
      }
    })
  }

  private def moveFixedComponents() = {
    this.updateBackgroundOnMovement()
    if (this.xPosition >= 0 && this.xPosition <= 4600) {
      this.moveObjects()
      this.moveMoney()
      this.moveEnemies()
    }
  }

  private def moveObjects() = {
    this.objects foreach (`object` => `object`.moveScene())
  }

  private def moveMoney() = {
    this.money foreach (m => m.moveScene())
  }

  private def moveEnemies() = {
    this.enemies foreach (enemy => {
      enemy.moveScene()
    })
  }

  private def drawBackgrounds(graphic: Graphics) = {
    graphic.drawImage(this.imageBackground1, this.background1XPosition, 0, null)
    graphic.drawImage(this.imageBackground2, this.background2XPosition, 0, null)
  }

  private def drawCastle(graphic: Graphics) = graphic.drawImage(this.castle, 10 - this.xPosition, 95, null)

  private def drawStart(graphic: Graphics) = graphic.drawImage(this.start, 220 - this.xPosition, 234, null)

  private def drawObjects(graphic: Graphics) = {
    this.objects foreach ( `object` => graphic.drawImage(`object`.imageObject, `object`.x, `object`.y, null))
  }

  private def drawMoney(graphic: Graphics) = {
    this.money foreach (m => graphic.drawImage(m.imageOnMovement(), m.x, m.y, null))
  }

  private def drawImageFlag(graphic: Graphics) = graphic.drawImage(this.imageFlag, FLAG_X_POS - this.xPosition, FLAG_Y_POS, null)

  private def drawImageCastle(graphic: Graphics) = graphic.drawImage(this.imageCastle, CASTLE_X_POS - this.xPosition, CASTLE_Y_POS, null)

  private def drawScore(graphic: Graphics) = graphic.drawString(Res.SCORE + this.score.getScore, SCORE_X_POSITION, SCORE_Y_POSITION)

  private def drawMario(graphic: Graphics) = {
    if (this.mario.isJumping) graphic.drawImage(this.mario.doJump(), this.mario.x, this.mario.y, null)
    else graphic.drawImage(this.mario.walk(if (this.mario.lives == 2) Res.IMGP_CHARACTER_MARIO else Res.IMGP_CHARACTER_MARIO_MINI , MARIO_FREQUENCY), this.mario.x, this.mario.y, null)
  }

  private def drawEnemies(graphic: Graphics) = {
    this.enemies foreach (enemy => { enemy match {
        case enemy: Mushroom =>
          if (enemy.isAlive) graphic.drawImage(enemy.walk(Res.IMGP_CHARACTER_MUSHROOM, MUSHROOM_FREQUENCY), enemy.x, enemy.y, null)
          else graphic.drawImage(enemy.deadImage, enemy.x, enemy.y + MUSHROOM_DEAD_OFFSET_Y, null)
        case enemy: Turtle =>
          if (enemy.isAlive) graphic.drawImage(enemy.walk(Res.IMGP_CHARACTER_TURTLE, TURTLE_FREQUENCY), enemy.x, enemy.y, null)
          else graphic.drawImage(enemy.deadImage, enemy.x, enemy.y + TURTLE_DEAD_OFFSET_Y, null)
      }
    })
  }

  private def checkMarioDeath(graphic: Graphics) = {
    if (!this.mario.isAlive) {
      Main.refresh.stopGame()
      this.score.reset()
      Audio playSound Res.AUDIO_GAME_OVER
      graphic.drawImage(this.imageGameOver, this.mario.x - GAME_OVER_X_POSITION, GAME_OVER_Y_POSITION, null)
      graphic.drawImage(this.imageTryAgain, this.mario.x - TRY_AGAIN_X_POSITION, TRY_AGAIN_Y_POSITION, null)
    }
  }

  override def updateScore(strategy: ScoreStrategy)= {
    this.score setScoreStrategy strategy
    this.score.increment()
  }
}