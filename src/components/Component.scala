package components

/**
  * Created by Giulia on 12/03/2017.
  */
trait Component {
  def x: Int

  def x_=(x: Int): Unit

  def y: Int

  def y_=(y: Int): Unit

  def width: Int

  def width_=(width: Int): Unit

  def height: Int

  def height_=(height: Int): Unit

  def moveScene(): Unit
}

class GameComponent(override var x: Int, override var y: Int, override var width: Int, override var height: Int) extends Component {

  import game.Main
  override def moveScene() = if (Main.scene.xPosition >= 0) this.x = this.x - Main.scene.movement
}

object Component {
  def apply(x: Int, y: Int, width: Int, height: Int):Component = new GameComponent (x, y, width, height)
}