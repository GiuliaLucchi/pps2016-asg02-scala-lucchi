package components.characters;

import components.objects.Money;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Giulia on 16/03/2017.
 */
public class MarioTest {

    private final static int X_POSITION = 0;
    private final static int Y_POSITION = 0;
    private Mario mario;
    private Money money;
    private Enemy mushroom;

    @Before
    public void init(){
        this.mario = new MarioImpl(X_POSITION, Y_POSITION);
        this.money = new Money(X_POSITION, Y_POSITION);
        this.mushroom = new Mushroom(X_POSITION, Y_POSITION);
    }

    @Test
    public void isMarioJumping() throws Exception {
        this.mario.isJumping_$eq(true);
        assertTrue(this.mario.isJumping());
    }

    @Test
    public void isMarioCollectingAMoney() throws Exception {
        assertTrue(this.mario.checkContact(this.money));
    }

    @Test
    public void isMarioDeadWhenInContactForTheSecondTimeWithAnEnemy() throws Exception {
        this.isMarioSmallWhenInContactForTheFirtsTimeWithAnEnemy();
        Thread.sleep(2000);
        this.mario.x_$eq(0);
        this.mario.y_$eq(0);
        this.mushroom.x_$eq(0);
        this.mario.contactWithCharacter(mushroom);
        assertFalse(this.mario.isAlive());
    }

    @Test
    public void isMarioSmallWhenInContactForTheFirtsTimeWithAnEnemy() throws Exception {
        assertTrue(this.mario.isAlive());
        this.mario.contactWithCharacter(mushroom);
        assertTrue(this.mario.lives()== 1);
    }

}